import { defineStore } from 'pinia'
import { computed, ref } from 'vue'

type Product = {
  id: string
  name: string
  price: number
  category: string
  urlImg: string
}

type ProductWithQty = {
  product: Product
  qty: number
}

export const useSelectionTab = defineStore('tabs', () => {
  const items = ref<ProductWithQty[]>([])

  function addItem(item: Product) {
    const i = <ProductWithQty>{
      product: item,
      qty: 1
    }
    items.value.push(i)
  }

  function removeItem(item: ProductWithQty) {
    const index = items.value.indexOf(item)
    if (index !== -1) {
      items.value.splice(index, 1)
    }
  }

  function clearItem() {
    items.value.splice(0, items.value.length)
  }
  const total = computed(() =>
    items.value.reduce((accumulator, item) => accumulator + item.product.price * item.qty, 0)
  )

  const processState = ref(false)

  function makeProcess() {
    if (items.value.length > 0) {
      processState.value = true
      clearItem()
    }

    setTimeout(() => {
      processState.value = false
    }, 3000)
  }

  function updateQuantity(item: ProductWithQty) {
    item.qty = Math.max(1, Math.floor(item.qty))
  }

  return { items, addItem, removeItem, clearItem, total, processState, makeProcess, updateQuantity }
})
